
class BranchNamer {

    private prefix: HTMLInputElement = <HTMLInputElement>document.querySelector('.prefix');
    private ticket: HTMLInputElement = <HTMLInputElement>document.querySelector('.ticket');
    private with: HTMLInputElement = <HTMLInputElement>document.querySelector('.with');
    private description: HTMLInputElement = <HTMLInputElement>document.querySelector('.description');
    private output: HTMLElement = <HTMLElement>document.querySelector('.output');

    private inputsForSave: Array<string> = [
        'prefix',
        'with'
    ];

    constructor() {
        this.init();
    }

    private init(): void {
        this.resumeValues();
        this.setHandlers();
    }

    private resumeValues(): void {
        this.inputsForSave.forEach((name: string): void => {
            let val = localStorage.getItem('name');
            if (val) {
                this[name].value = val;
            }
        });
    }

    private setHandlers(): void {



    }

}
